﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultipluMatrix
{
    class MultiplicationMatrix<T>
    {
        public static Matrix<T> Myltiply(Matrix<T> matrix1,Matrix<T> matrix2)
        {
            Matrix<T> res = new Matrix<T>();

            Thread[] threads = new Thread[matrix1.countRow];
            List<Vector<T>> resVector = new List<Vector<T>>();

            for (int i = 0; i < matrix1.countRow; i++)
            {
                resVector.Add(matrix1.GetRow(i).MultipliedByMatrix(matrix2));

                //resVector.Add(new Vector<T>(matrix2.countRow));
                //threads[i] = new Thread(()=> resVector[i] = matrix1.GetRow(i).MultipliedByMatrix(matrix2));
                //threads[i].Name = string.Format("Работает поток: #{0}", i);
            }
            //for(int i=0;i<matrix1.countRow;i++)
            //{
            //    threads[i].Start();
            //    threads[i].Join();
            //}

            for(int i = 0;i<matrix1.countRow;i++)
            {
                res.AddRow(resVector[i]);
            }
            return res;
        }
    }
}
