﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipluMatrix
{
    class Matrix<T>
    {
        public int countRow { get; set; }

        public int countColume { get; set; }

        public List<List<T>> matrix { get; set; }

        public Matrix()
        {
            matrix = new List<List<T>>();
            countColume = 0;
            countRow = 0;
        }

        public Matrix(List<List<T>> _matrix)
        {
            matrix = _matrix;
            countRow = _matrix.Count;
            try
            {
                countColume = _matrix[0].Count;
            }catch
            {
                countColume = 0;
            }
        }

        public T this[int iIndex,int jIndex]
        {
            get
            {
                return matrix[iIndex][jIndex];
            }
            set
            {
                matrix[iIndex][jIndex] = value;
            }
        }

        public Vector<T> GetRow(int indexRow)
        {
            return new Vector<T>(matrix[indexRow]);
        }
        public void AddRow(Vector<T> _vector)
        {
            matrix.Add(_vector.ToList());
        }
        public void Print()
        {
            Console.WriteLine("Count row = " + countRow + " count colume = " + countColume);
            for (int i = 0; i < countRow; i++)
            {
                for (int j = 0; j < countColume; j++)
                {
                    Console.Write(matrix[i][j]+" ");
                }
                Console.WriteLine();
            }
        }
    }
}
