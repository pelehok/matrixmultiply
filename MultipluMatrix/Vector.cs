﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipluMatrix
{
    class Vector<T>
    {
        public int count { get; set; }
        public List<T> vector { get; set; }
        public Vector()
        {
            vector = new List<T>();
            count = 0;
        }
        public void Add(T _value)
        {
            vector.Add(_value);
            count++;
        }
        public Vector(List<T> _vector)
        {
            count = _vector.Count;
            vector = _vector;
        }
        public Vector<T> MultipliedByMatrix(Matrix<T> _matrix)
        {
            Vector<T> res = new Vector<T>();

            for(int i=0;i<_matrix.countColume;i++)
            {
                T sum = default(T);
                for(int j=0;j<_matrix.countRow;j++)
                {
                    var Value = _matrix[j, i] as dynamic;
                    sum += (vector[j] * Value);
                }
                res.Add(sum);
            }
            return res;
        }

        public T this[int index]
        {
            get
            {
                return vector[index];
            }
            set
            {
                vector[index] = value;
            }
        }

        public List<T> ToList()
        {
            List<T> res = new List<T>();
            for(int i=0;i<count;i++)
            {
                res.Add(vector[i]);
            }
            return res;
        }

        public void Print()
        {
            for(int i=0;i<count;i++)
            {
                Console.Write(vector[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
