﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipluMatrix
{
    class RandomMatrix
    {
        public static Matrix<int> Create(int countRow, int countColume,int diapazonA,int diapazinB)
        {
            List<List<int>> res = new List<List<int>>();

            Random random = new Random();
            for(int i =0;i<countRow;i++)
            {
                res.Add(new List<int>());
                for(int j=0;j<countColume;j++)
                {
                    res[i].Add(random.Next(diapazonA, diapazinB));
                }
            }
            
            return new Matrix<int>(res);
        }
    }
}
